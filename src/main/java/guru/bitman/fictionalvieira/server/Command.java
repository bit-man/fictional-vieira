package guru.bitman.fictionalvieira.server;

import java.io.Writer;

/***
 * Command interface
 */
public interface Command {
	/***
	 * Execute command
	 * @param out : writer
	 */
	void execute(Writer out);
}
