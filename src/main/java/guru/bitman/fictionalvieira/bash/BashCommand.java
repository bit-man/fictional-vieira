package guru.bitman.fictionalvieira.bash;

public interface BashCommand
{
    String getCode();
}
